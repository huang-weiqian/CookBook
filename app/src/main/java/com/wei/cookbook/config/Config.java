package com.wei.cookbook.config;

/**
 * 作者：赵若位
 * 时间：2018/3/27 17:08
 * 邮箱：1070138445@qq.com
 * 功能：
 */

public class Config
{
    /*App闪屏倒计时*/
    public static final long SPLASH_DELAY_TIME = 1500;
    /*API请求KEY*/
    public static final String HTTP_NET_KEY = "5897c1b3b4a43e68fb35dddada8d1cf5";
    /*用户评论统一头像*/
    public static final String BASE_HEADE = "https://qlogo4.store.qq.com/qzone/986823663/986823663/100";
}
